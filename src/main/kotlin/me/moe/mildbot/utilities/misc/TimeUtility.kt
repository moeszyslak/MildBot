package me.moe.mildbot.utilities.misc

import org.joda.time.format.DateTimeFormat

fun formatTime(time: Long): String{
    val days = time / (1000 * 60 * 60 * 24)
    val hours = time / (1000 * 60 * 60)
    val minutes = time / (1000 * 60)
    val seconds = time / 1000
    return "$days day(s) $hours hour(s) $minutes minute(s) and $seconds second(s)"
}

val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")!!