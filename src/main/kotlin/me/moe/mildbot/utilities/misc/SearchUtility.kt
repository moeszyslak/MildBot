package me.moe.mildbot.utilities.misc

import me.aberrantfox.kjdautils.api.dsl.embed
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color

data class SearchSettings(val searchEngine: String,
                          val apiKey: String)

fun failedSearch(name: String): MessageEmbed {
    return embed {
        setColor(Color.RED)
        setTitle("Failed to search for $name try again later")

    }
}

fun failedStatusSearch(website: String): MessageEmbed {
    return embed {
        setColor(Color.RED)
        setTitle("Failed to check the status of $website try again later")
    }
}

fun urlify(searchTerm: String): String{
    return searchTerm.replace(" ", "+")
}

