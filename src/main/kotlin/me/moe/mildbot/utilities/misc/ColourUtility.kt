package me.moe.mildbot.utilities.misc

import me.moe.mildbot.utilities.Configuration
import java.awt.Color

data class Colours(val utilityColour: Color,
                   val googleColour: Color,
                   val mdnColour: Color,
                   val githubColour: Color,
                   val dictionaryColor: Color)

fun stringToColour(colourStr: String): Color {
    return try {
        val colour = Color.decode(colourStr)!!
        colour
    } catch (e: NumberFormatException) {
        Color.WHITE
    }
}

fun loadColours(config: Configuration): Colours{
    return Colours(
            stringToColour(config.colour.utilityColour),
            stringToColour(config.colour.googleColour),
            stringToColour(config.colour.mdnColour),
            stringToColour(config.colour.githubColour),
            stringToColour(config.colour.dictionaryColour)
    )
}
