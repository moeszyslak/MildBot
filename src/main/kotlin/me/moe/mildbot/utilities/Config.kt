package me.moe.mildbot.utilities

import com.google.gson.GsonBuilder
import java.io.File

open class Configuration(val connection: Connection = Connection(),
                         val colour: Colour = Colour(),
                         val searchAPI: SearchAPI = SearchAPI())

data class Connection(val token: String = "bot-token",
                      val prefix: String = "prefix")

data class Colour(val utilityColour: String = "#FFFFFF",
                  val googleColour: String = "#34a853",
                  val mdnColour: String = "#000000",
                  val githubColour: String = "#24292e",
                  val dictionaryColour: String = "#337ab7")

data class SearchAPI(val searchEngine: String = "Search Engine ID",
                     val apiKey: String = "API key")

private val gson = GsonBuilder().setPrettyPrinting().create()
private val configDir = File("config/")
private val configFile = File("config/config.json")

fun loadConfig(): Configuration? {
    if(!(configFile.exists())){
        configDir.mkdirs()
        configFile.writeText(gson.toJson(Configuration()))
        return null
    }
    return gson.fromJson(configFile.readText(), Configuration::class.java)
}

fun saveConfig(config: Configuration?) = configFile.writeText(gson.toJson(config))