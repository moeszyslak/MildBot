package me.moe.mildbot

import me.aberrantfox.kjdautils.api.startBot
import me.moe.mildbot.utilities.loadConfig
import me.moe.mildbot.utilities.misc.SearchSettings
import me.moe.mildbot.utilities.misc.loadColours
import me.moe.mildbot.utilities.saveConfig

fun main(args: Array<String>){
    val config = loadConfig()

    if(config == null){
        println("A config file has been created in config/config.json, fill out all required fields and re-run")
        return
    }

    saveConfig(config)

    val token = System.getenv("BOT_TOKEN") ?: config.connection.token
    val searchAPI = SearchSettings(
            System.getenv("SEARCH_ENGINE_ID") ?: config.searchAPI.searchEngine,
            System.getenv("SEARCH_API") ?: config.searchAPI.apiKey)

    startBot(token) {
        val colours = loadColours(config)
        val prefix = config.connection.prefix
        val commandPath = "me.moe.mildbot.commands"

        registerInjectionObject(colours, config, searchAPI)
        registerCommands(commandPath, prefix)
    }
}