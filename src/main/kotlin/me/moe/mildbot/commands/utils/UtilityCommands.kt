package me.moe.mildbot.commands.utils

import com.google.gson.Gson
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.moe.mildbot.utilities.misc.Colours
import me.moe.mildbot.utilities.misc.formatTime
import java.util.*


data class Properties(val author: String, val name: String, val version: String, val kotlinVersion: String,
                      val jdaVersion: String, val kutilsVersion: String, val url: String)

object Project{
    val properties: Properties

    init {
        val propertiesFile = this::class.java.getResource("/properties.json").readText()
        val gson = Gson()
        properties = gson.fromJson(propertiesFile, Properties::class.java)
    }
}

val botStarted = Date()

@CommandSet
fun utilityCommands(colours: Colours) = commands{

    command("ping"){
        execute {
            it.respond(embed{
                setColor(colours.utilityColour)
                setTitle("Responded in")
                setDescription("${it.jda.ping}ms")
            })
        }
    }

    command("version"){
        execute {
            it.respond(embed{
                setColor(colours.utilityColour)

                field {
                    name = "MildBot version"
                    value = Project.properties.version
                }

                field {
                    name = "Kotlin version"
                    value = Project.properties.kotlinVersion
                }

                field {
                    name = "JDA version"
                    value = Project.properties.jdaVersion
                }

                field{
                    name = "KUtils version"
                    value = Project.properties.kutilsVersion
                }
            })
        }
    }

    command("source"){
        execute {
            it.respond(embed{
                setColor(colours.utilityColour)

                setTitle("${Project.properties.name} version ${Project.properties.version}")
                setDescription(Project.properties.url)
            })
        }
    }

    command("uptime"){
        execute {
            val difference = Date().time - botStarted.time
            it.respond(embed{
                setColor(colours.utilityColour)
                setTitle("MildBot has been online for")
                setDescription(formatTime(difference))
            })
        }
    }
}