package me.moe.mildbot.commands.search

import khttp.get
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.SentenceArg
import me.moe.mildbot.utilities.misc.Colours
import me.moe.mildbot.utilities.misc.failedSearch
import me.moe.mildbot.utilities.misc.urlify
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color

@CommandSet
fun dictionaryCommands(colours: Colours) = commands {
    command("dictionary") {
        expect(SentenceArg)
        execute {
            val query = it.args.first() as String

            it.respond(definitionSearch(query, colours.dictionaryColor))
        }
    }

    command("define") {
        expect(SentenceArg)
        execute {
            val query = it.args.first() as String

            it.respond(definitionSearch(query, colours.dictionaryColor))
        }
    }
}

private fun definitionSearch(query: String, dictionaryColour: Color): MessageEmbed {
    try {
        val response = get("https://owlbot.info/api/v2/dictionary/${urlify(query)}?format=json")

        return if (response.statusCode != 200) {
            failedSearch(query)
        } else {
            val firstResult = response.jsonArray.getJSONObject(0)
            val type = firstResult.getString("type")
            val definition = firstResult.getString("definition")
            val example = firstResult.getString("example")

            embed {
                setColor(dictionaryColour)
                setTitle("Dictionary search results for $query")

                field {
                    name = "Definition"
                    value = definition
                    inline = false
                }

                field {
                    name = "Type"
                    value = type
                    inline = false
                }

                field {
                    name = "Example"
                    value = example
                    inline = false
                }

            }
        }
    } catch (e: Exception) {
        return failedSearch(query)
    }


}