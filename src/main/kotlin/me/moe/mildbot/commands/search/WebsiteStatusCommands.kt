package me.moe.mildbot.commands.search

import khttp.get
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.WordArg
import me.moe.mildbot.utilities.misc.failedStatusSearch
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color

@CommandSet
fun websiteStatusCommands() = commands {
    command("isitdown") {
        expect(WordArg)
        execute {
            val website = it.args.first() as String

            it.respond(statusResponse(website))
        }
    }
}

private fun statusResponse(website: String): MessageEmbed {
    try {
        val response = get("https://isitdown.site/api/$website")

        return if (response.statusCode != 200) {
            failedStatusSearch(website)
        } else {
            val status = response.jsonObject.getBoolean("isitdown")

            embed {
                if (status) {
                    setTitle("$website is either down or doesn't exist")
                    setColor(Color.RED)
                } else {
                    setTitle("$website is up, any issues are on your device")
                    setColor(Color.GREEN)
                }
            }
        }
    } catch (e: Exception) {
        return failedStatusSearch(website)
    }
}
