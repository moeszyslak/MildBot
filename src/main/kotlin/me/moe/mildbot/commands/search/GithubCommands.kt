package me.moe.mildbot.commands.search

import khttp.get
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.SentenceArg
import me.moe.mildbot.utilities.misc.Colours
import me.moe.mildbot.utilities.misc.dateFormat
import me.moe.mildbot.utilities.misc.failedSearch
import me.moe.mildbot.utilities.misc.urlify
import net.dv8tion.jda.core.entities.MessageEmbed
import org.joda.time.Instant
import org.json.JSONException
import java.awt.Color

@CommandSet
fun githubCommands(colours: Colours) = commands {
    command("github") {
        expect(SentenceArg)
        execute {
            val query = it.args.first() as String

            it.respond(githubSearch(query, colours.githubColour))
        }
    }
}

private fun githubSearch(query: String, githubColour: Color): MessageEmbed {
    try {
        val response = get("https://api.github.com/search/repositories?q=${urlify(query)}")

        return if (response.statusCode != 200) {
            failedSearch(query)
        } else {
            val firstResult = response.jsonObject.getJSONArray("items").getJSONObject(0)
            val resultName = firstResult.getString("name")
            val author = firstResult.getJSONObject("owner").getString("login")
            val url = firstResult.getString("html_url")
            val desc =
                    try {
                        firstResult.getString("description")
                    } catch (e: JSONException) {
                        "No description provided"
                    }
            val stars = firstResult.getInt("watchers").toString()
            val forks = firstResult.getInt("forks").toString()
            val language =
                    try {
                        firstResult.getString("language")
                    } catch (e: JSONException) {
                        "No language provided"
                    }
            val lastUpdated = dateFormat.print(Instant.parse(firstResult.getString("updated_at")))


            print(resultName + author + url)
            embed {
                setColor(githubColour)
                setAuthor("Github search results for $query", url, "https://i.imgur.com/dkkMZl1.png")
                setTitle(resultName, url)
                field {
                    name = "Description"
                    value = desc
                    inline = false
                }

                field {
                    name = "Author"
                    value = author
                    inline = false
                }

                field {
                    name = "Stars"
                    value = stars
                    inline = true
                }

                field {
                    name = "Forks"
                    value = forks
                    inline = true
                }

                field {
                    name = "Language"
                    value = language
                    inline = true
                }

                field {
                    name = "Last updated (UTC)"
                    value = lastUpdated
                    inline = true
                }
            }
        }

    } catch (e: Exception) {
        return failedSearch(query)
    }

}