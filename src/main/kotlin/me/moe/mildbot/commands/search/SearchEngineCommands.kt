package me.moe.mildbot.commands.search

import khttp.get
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.SentenceArg
import me.moe.mildbot.utilities.misc.Colours
import me.moe.mildbot.utilities.misc.SearchSettings
import me.moe.mildbot.utilities.misc.failedSearch
import me.moe.mildbot.utilities.misc.urlify
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color

@CommandSet
fun searchEngineCommands(colours: Colours, searchSettings: SearchSettings) = commands {
    command("google"){
        expect(SentenceArg)

        execute{
            val query = it.args.first() as String

            it.respond(googleSearch(query, colours.googleColour, searchSettings))
        }
    }
}

private fun googleSearch(query: String, googleColour: Color, searchSettings: SearchSettings): MessageEmbed {
    try{
        val response = get(
                "https://www.googleapis.com/customsearch/v1?" +
                        "q=${urlify(query)}&cx=${searchSettings.searchEngine}&key=${searchSettings.apiKey}")

        return if (response.statusCode != 200){
            failedSearch(query)
        } else {
            val firstResult = response.jsonObject.getJSONArray("items").getJSONObject(0)
            val title = firstResult.getString("title")
            val link = firstResult.getString("link")
            val snippet = firstResult.getString("snippet")

            embed {
                setColor(googleColour)
                setAuthor("Google search results for $query", link, "https://i.imgur.com/Y4TAD7V.png")
                setTitle(title, link)
                setDescription(snippet)
            }
        }

    } catch (e: Exception){
        return failedSearch(query)
    }
}