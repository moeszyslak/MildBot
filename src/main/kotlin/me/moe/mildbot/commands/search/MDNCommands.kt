package me.moe.mildbot.commands.search

import khttp.get
import khttp.responses.Response
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.SentenceArg
import me.moe.mildbot.utilities.misc.Colours
import me.moe.mildbot.utilities.misc.failedSearch
import me.moe.mildbot.utilities.misc.urlify
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color
import java.net.URISyntaxException

@CommandSet
fun mdnCommands(colours: Colours) = commands {

    command("javascript"){
        expect(SentenceArg)
        execute{
            val query = it.args.first() as String

            it.respond(javascriptSearch(query, colours.mdnColour))
        }
    }

    command("js"){
        expect(SentenceArg)
        execute{
            val query = it.args.first() as String

            it.respond(javascriptSearch(query, colours.mdnColour))
        }
    }

    command("html"){
        expect(SentenceArg)
        execute {
            val query = it.args.first() as String

            it.respond(htmlSearch(query, colours.mdnColour))
        }
    }

    command("css"){
        expect(SentenceArg)
        execute {
            val query = it.args.first() as String

            it.respond(cssSearch(query, colours.mdnColour))
        }
    }
}


private fun javascriptSearch(search: String, mdnColour: Color): MessageEmbed {

    return try {
        val response = get(
                "https://developer.mozilla.org/en-US/search.json?q=${urlify(search)}&topic=javascript&topic=js")

        buildMdnEmbed(search, response, mdnColour)
    } catch (e: URISyntaxException) {
        failedSearch(search)
    }
}

private fun htmlSearch(search: String, mdnColour: Color): MessageEmbed {

    return try {
        val response = get(
                "https://developer.mozilla.org/en-US/search.json?q=${urlify(search)}&topic=html")

        buildMdnEmbed(search, response, mdnColour)
    } catch (e: URISyntaxException) {
        failedSearch(search)
    }
}

private fun cssSearch(search: String, mdnColour: Color): MessageEmbed {

    return try {
        val response = get(
                "https://developer.mozilla.org/en-US/search.json?q=${urlify(search)}&topic=css")

        buildMdnEmbed(search, response, mdnColour)
    } catch (e: URISyntaxException) {
        failedSearch(search)
    }
}

private fun buildMdnEmbed(search: String, response: Response, mdnColour: Color): MessageEmbed {

    try{
        return if(response.statusCode != 200){
            failedSearch(search)
        } else {
            val json = response.jsonObject.getJSONArray("documents").getJSONObject(0)
            val name = json.getString("title")
            val url = json.getString("url")
            val excerpt = json.getString("excerpt")

            embed {
                setColor(mdnColour)
                setAuthor("MDN search results for $search", url, "https://i.imgur.com/pcPLsrb.png")
                setTitle(name, url)
                setDescription(excerpt)
            }
        }
    } catch (e: Exception){
        return failedSearch(search)
    }
}

